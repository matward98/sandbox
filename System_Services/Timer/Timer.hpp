//
//  Timer.hpp
//  System_Services
//
//  Created by Matthew Ward on 2023-12-01.
//

#ifndef Timer_hpp
#define Timer_hpp

#include <chrono>

namespace Timer {
    typedef struct Two_Field_Time {
        std::chrono::seconds Seconds;
        std::chrono::nanoseconds Nanoseconds;
        
        Two_Field_Time(std::chrono::seconds Sec, std::chrono::nanoseconds nS)
        {
            Seconds = Sec;
            Nanoseconds = nS;
        }
    } Two_Field_Time;
    
    class Timer_Class
    {
        void(*Callback_Function)(void*);
        void* Timer_Data;
        bool Continue;
        bool Running;
        std::chrono::nanoseconds Duration;
        
        void Thread_Func();
    public:
        
        Timer_Class();
        void Set_Callback(void(*Func)(void*));
        void Set_Data(void* Data);
        int Start(Two_Field_Time Dur);
        bool Is_Running();
        void Stop();
    };
}
#endif /* Timer_hpp */
