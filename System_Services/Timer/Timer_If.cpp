//
//  Timer_If.cpp
//  System_Services
//
//  Created by Matthew Ward on 2023-12-01.
//
#include <thread>
#include <map>

#include "Timer_If.hpp"

using namespace Timer_If;

namespace {
    Timer_If::Timer_Id_Type Id_Counter = 0;
}
    
Timer_Wrapper::Timer_Wrapper(Timer::Timer_Class &Timer_In) :
Timer_Id(++Id_Counter),
Timer(Timer_In)
{
}

void Timer_Wrapper::Thread_Func(Timer::Two_Field_Time Dur)
{
    Timer_Id = Timer.Start(Dur);
}

Timer_If::Timer_Id_Type Timer_Wrapper::Start(Timer::Two_Field_Time Dur)
{
    std::thread([this,Dur](){Thread_Func(Dur);});
    return Timer_Id;
}

Timer_If::Timer_Id_Type Timer_Wrapper::Get_Id()
{
    return Timer_Id;
}

void Timer_Wrapper::Stop()
{
    Timer.Stop();
}

namespace {
    std::map<Timer_If::Timer_Id_Type, Timer_Wrapper> Id_Map;
    bool Cleaner_Running = false;
}

void Run_Cleaner()
{
    Cleaner_Running = true;
    while(Id_Map.size() > 0)
    {
        for(auto &[key, value] : Id_Map)
        {
            if (key != value.Get_Id()) {
                Id_Map.erase(key);
            }
        }
    }
    Cleaner_Running = false;
}

Timer_Id_Type Start_Timer(void(* Func)(void *), void* Data, Timer::Two_Field_Time tim)
{
    Timer::Timer_Class New_Timer;
    New_Timer.Set_Callback(Func);
    New_Timer.Set_Data(Data);
    
    Timer_Wrapper Wrapper(New_Timer);
    Timer_Id_Type New_Id = Wrapper.Start(tim);
    Id_Map.insert({New_Id, Wrapper});
    if(Cleaner_Running == false)
    {
        std::thread Cleaner(Run_Cleaner);
    }
    return New_Id;
}

void Stop_Timer(Timer_Id_Type Id)
{
    Timer_Wrapper& Wrapper = Id_Map.at(Id);
    if(Wrapper.Get_Id() == Id)
    {
        Wrapper.Stop();
    }
    else
    {
        Id_Map.erase(Id);
    }
}
