//
//  Timer_If.hpp
//  System_Services
//
//  Created by Matthew Ward on 2023-12-01.
//

#ifndef Timer_If_hpp
#define Timer_If_hpp

#include <stdio.h>
#include "Timer.hpp"

namespace Timer_If {
    typedef uint64_t Timer_Id_Type;
    typedef Timer::Two_Field_Time Two_Field_Time;
    
    class Timer_Wrapper
    {
        Timer_If::Timer_Id_Type Timer_Id;
        Timer::Timer_Class &Timer;
        void Thread_Func(Timer::Two_Field_Time Dur);
    public:
        Timer_Wrapper(Timer::Timer_Class &Timer_In);
        Timer_If::Timer_Id_Type Start(Timer::Two_Field_Time Dur);
        Timer_If::Timer_Id_Type Get_Id();
        void Stop();
    };
    
    Timer_Id_Type Start_Timer(void(* Func)(void *), void* Data, Timer::Two_Field_Time tim);
    void Stop_Timer(Timer_Id_Type Id);
}

#endif /* Timer_If_hpp */
