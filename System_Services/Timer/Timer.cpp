//
//  Timer.cpp
//  System_Services
//
//  Created by Matthew Ward on 2023-12-01.
//

#include <thread>

#include "Timer.hpp"

using namespace Timer;

Timer_Class::Timer_Class() :
Callback_Function(nullptr),
Timer_Data(nullptr),
Continue(true),
Running(false),
Duration(0)
{}

void Timer_Class::Set_Callback(void(*Func)(void *))
{
    Callback_Function = Func;
}

void Timer_Class::Set_Data(void* Data)
{
    Timer_Data = Data;
}

void Timer_Class::Thread_Func()
{
    std::this_thread::sleep_for(Duration);
    if (Continue) {
        Callback_Function(Timer_Data);
    }
    Running = false;
}

int Timer_Class::Start(Two_Field_Time Dur)
{
    Duration = Dur.Seconds;
    Duration += Dur.Nanoseconds;
    Running = true;
    Continue = true;
    std::thread _Thread(&Timer_Class::Thread_Func,this);
    _Thread.join();
    this->~Timer_Class();
    return 0;
}

bool Timer_Class::Is_Running()
{
    return Running;
}

void Timer_Class::Stop()
{
    Continue = false;
}
