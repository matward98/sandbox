//
//  main.cpp
//  Timer_Test
//
//  Created by Matthew Ward on 2024-03-02.
//

#include <iostream>
#include <thread>
#include <chrono>

#include "Timer_If.hpp"

void Test_Func(void* Test_Data)
{
    const char* Test_String = (const char*)(Test_Data);
    std::cout<<std::string(Test_String)<<std::endl;
}

int main(int argc, const char * argv[]) {
    Timer_If::Two_Field_Time Dur(std::chrono::seconds(10),std::chrono::nanoseconds(0));
    const char* Data = "Hello_World";
    std::cout<<"Starting timer"<<std::endl;
    Timer_If::Timer_Id_Type Id = Timer_If::Start_Timer(Test_Func, (char*) Data, Dur);
    std::cout<<"Should be threaded"<<std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(15));
    std::cout<<"Ending Test"<<std::endl;
    return 0;
}
